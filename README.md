## Utilisation du Markdown
Ce guide va vous apprendre à écrire en Markdown. Vous
pourrez ainsi rédiger de beaux documents à partir d'un fichier texte brut,
c'est à dire en limitant fortement le nombre de clics de souris: un gain de
temps substantiel. Dans ce guide on va créer un projet qui pourra vous servir de rappel. Nous allons pour cela créer un projet personnel nommé "MonMarkdown" contenant un fichier "README.md". Pour cela:

1. Maintenez la touche contrôle (*ctrl*) et cliquez sur [ce lien](https://framagit.org/users/sign_in) pour ouvrir le lien dans un nouvel onglet puis inscrivez vous en cliquant sur *Register* ou si vous êtes déjà inscrit connectez-vous.
2. Cliquez sur *Create a project* ou dans le cas ou vous êtes déjà inscrit vous pourrez cliquer sur *le plus* en haut à droite de la page puis *New project*.
3. Ecrivez "MonMarkdown" dans la case *Project name* et cochez la case *Initialize repository with a README*.

## Afficher un document Markdown (.md) en texte brut
Cliquez sur *README.md*, vous voyez alors le fichier qui a été lu par la plateforme avec un beau titre principal **Le language Markdown**, cliquez mainteant sur le bouton *Edit*. Vous devriez alors voir:

    # MonMarkdown

## Créez des titres et des sous-titres
Vous voyez à quoi ressemble un fichier Markdown en texte brut. Ce qui vous permet de voir comment génèrer des titres en Markdown: on les précède de \#. Créons maintenant ensemble un premier sous-titre.

Pour cela cliquez sur *Edit*, sautez une ligne puis écrivez:

    ## Ecrire le Markdown 

Enfin cliquez sur le bouton vert en bas de page *Commit Change*. *Bravo!* Vous venez d'utiliser Git en faisant un premier commit.
Vous allez en faire beaucoup d'autres en suivant à chaque fois la même procédure: un clic sur *Edit* puis on vous demandera d'écrire un texte et enfin un autre sur *Commit change*, on vous demandera de faire ces trois étapes quand on écrira **A vous!**.

## Accentuer des mots grâce au Markdown
Le Markdown permet d'accentuer les mots. On les entoure de part et d'autre d'un ou de deux \*:
    
    En Markdown on peut accentuer *Un peu* ou **Beaucoup**.

**A vous!**

## Organisation en paragraphes
Les paragraphes sont séparés par des lignes vides, c'est d'ailleurs pour cette raison que vous sautez des lignes entre chaque ajout:

    Un premier paragraphe.
    Cette phrase appartient toujours au premier paragraphe.
    
    Un second paragraphe.
    L'affichage sera le même si on sépare les paragraphes par 1 ou 10 lignes vides

**A vous!**
## Faire des listes
Le Markdown permet de faire des listes à puces en précédant les différents éléments de la liste d'un \- ou d'un nombre suivi d'un point puis d'un espace:

    - Une puce
    - Une autre puce

    1. Un premier élément
    2. Deuxième élément

**A vous!**

## Inclure des citations
Les \> permettent d'inclure des citations et plusieurs \> permettent d'y répondre:

    > Une citation

    >> Sa réponse

**A vous!**

## Inclure un caractère spécial
Il suffit de précéder un caratère spécial d'un \\ pour l'afficher dans le rendu final. On peut aussi insérer un bloc de texte non interprété en le précédent de quatre espaces et en sautant une ligne vide avant et après:

    Le \#, \* et le \\ sont des caractères spéciaux
    
         Un bloc non interprété 
         qui ne fait # pas de titre
    
**A vous!**

## Inclure des liens
On ajoute un lien en mettant le texte correspondant au lien entre \[ \] et sa cible entre \( \), attention on colle bien les crochets et les parenthèses, on peut utiliser la fonction copier/coller pour éviter les fautes de frappe:
    
    Ce document me permettra d'acceder à [Git](https://framagit.orgs/RomainC/git) 

**A vous!**
 
## Les images
Les images fonctionnent comme les liens sauf qu'on les précède d'un \!:
    
    ![Ce texte s'affiche en dessous ou à la place de l'image](https://framagit.org/uploads/-/system/project/avatar/69683/markdown.png)

**A vous!**   

## Conclusion
C'est le language Markdown qui nous a permis d'écrire ce guide et vous possédez maintenant un projet Git dont le README.md pourra vous servir de rappel en cas d'oubli!
Pour aller plus loin vous pouvez consultez ce [lien](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) en CC-BY comme ce tuto.

